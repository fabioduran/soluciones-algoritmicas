#include <stdio.h>
#define N 100
void ordIntercambio (int a[], int n){

    int i, j;

    /* se realizan n−1 pasadas */
    /* a[o], ... , a[n-2] */
    for (i = 0 ; i <= n-2 ; i++)
        /* coloca mínimo de a[i+1]...a[n-1] en a[i] */
        for (j = i+1 ; j <= n-1 ; j++)
            if (a[i] > a[j]){
                int aux;
                aux = a[i];
                a[i] = a[j];
                a[j]= aux ;
            }
}


void entradaLista (int a[], int n){

    int i;
    for (i = 0 ; i < n ; i++){
        char c;
        c = (i%10==0)?'\n':' ';
        printf("%c%d", c, a[i]);
    }
}

void imprimirLista (int a[], int n){

    int i;
    printf("\n Entrada de los elementos\n");
    for (i = 0 ; i < n ; i++){
        printf("a[%d] = ",a[i]);
        scanf("%d", a+i);
    }
}

int main(){
    int n;
    int v[N];

    do {
        printf("\nIntroduzca el número de elementos: ");
        scanf("%d", &n);
    } while ((n < 1) && (n > N));
        entradaLista(v, n);

    /* muestra lista original */
    printf("\nLista original de %d elementos", n);
    imprimirLista(v, n);

    /* ordenación ascendente de la lista */
    ordIntercambio(v, n);
    printf("\nLista ordenada de %d elementos", n);
    imprimirLista(v, n);
    return 0;
}
